let urlApi='https://dog.ceo/api/';
let razas=[];

document.addEventListener("DOMContentLoaded",function(){
  fetch(urlApi+'breeds/list/all')
  .then(respuesta=>{
    //console.log(respuesta);
    if(respuesta.statusText==='OK'){

      return respuesta.json();

		}

    alert("razas no encontradas");
    return "Not Found";

  })
  .then(datos=>{

    //console.log(datos.status);
    if(datos.status.localeCompare("success")===0){
      //console.log(datos.message);

      for(let key in datos.message){
        if(datos.message.hasOwnProperty(key)){
          // console.log(key+' -> '+datos.message[key]);
          // console.log(typeof datos.message[key]);
          let raza={
              nombre:key,
              subrazas:datos.message[key],
              imagen:''

          };

          razas.push(raza);
        }
      }

      //console.log(razas);

      let llamadas_imagenes=[];
      razas.forEach(raza=>{
        llamadas_imagenes.push(fetch(`${urlApi}breed/${raza.nombre}/images/random`).then(resp=>{return resp.json();}));
      });

      Promise.all(llamadas_imagenes)
      .then(imagenes_razas=>{
        //console.log(imagenes_razas);

        imagenes_razas.forEach(imagen_raza=>{
          if(imagen_raza.status.localeCompare("success")===0){
              let arreglo_palabras=imagen_raza.message.split("/");
              let nombre_desde_foto=arreglo_palabras[arreglo_palabras.length-2];
              razas.forEach(raza=>{
                // if(raza.nombre.localeCompare(nombre_desde_foto)===0){
                //   raza.imagen=imagen_raza.message;
                // }

                if(nombre_desde_foto.startsWith(raza.nombre)){
                  raza.imagen=imagen_raza.message;
                }

              });
          }

        });

        console.log(razas);
        /*pongamos los datos en los html*/



        // let ulRazas= $('<ul></ul>');
        //
        // razas.forEach(raza=>{
        //   let liRaza= $('<li></li>');
        //   let h2_raza=$('<h2></h2>');
        //   h2_raza.text(raza.nombre);
        //   let imagen_raza=$('<img >')
        //   imagen_raza.attr('src', raza.imagen);
        //   liRaza.append(h2_raza);
        //   liRaza.append(imagen_raza);
        //   ulRazas.append(liRaza);
        //
        // });
        // $('#lista-razas').append(ulRazas);

        /*VERSION CON BOOTSTRAP*/

        let acordeon=$('<div></div>').attr('id','acordeonRazas').addClass("accordion");
        let letras=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

        letras.forEach(letra=>{

          //console.log(letra);

          let carta=$('<div></div>').addClass("card");

          let encabezado=$('<div></div>').attr('id',`encabezado-letra-${letra}`).addClass("card-header");
          let h2_encabezado=$('<h2></h2>').addClass("mb-0");
          let boton_colapsar=$('<button></button>').attr( { type:"button", "data-toggle":"collapse", "data-target":`#collapsoLetra-${letra}`  ,"aria-expanded":"false","aria-controls":`collapsoLetra-${letra}` } ).addClass("btn btn-link btn-block text-left");
          boton_colapsar.text(letra.toUpperCase());

          h2_encabezado.append(boton_colapsar);
          encabezado.append(h2_encabezado);
          carta.append(encabezado);



          let grupo_colapsable=$('<div></div>').attr({id:`collapsoLetra-${letra}`, "aria-labelledby":`encabezado-letra-${letra}`, "data-parent":'#acordeonRazas'}).addClass("collapse");

          let cuerpo_carta=$('<div></div>').addClass("card-body");
          //cuerpo_carta.text("acá debiesen ir los perros");

          razas.forEach(raza=>{

            if(raza.nombre.startsWith(letra)){
              let div_raza=$('<div></div>');
              let h2_raza=$('<h2></h2>');
              h2_raza.text(raza.nombre);
              let imagen_raza=$('<img >')
              imagen_raza.attr('src', raza.imagen);
              div_raza.append(h2_raza);
              div_raza.append(imagen_raza);
              cuerpo_carta.append(div_raza);

            }


          });

          grupo_colapsable.append(cuerpo_carta);
          carta.append(grupo_colapsable);

          acordeon.append(carta);





        });

        $('#lista-razas').append(acordeon);

  //       <div class="card">
  //   <div class="card-header" id="headingOne">
  //     <h2 class="mb-0">
  //       <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
  //         Collapsible Group Item #1
  //       </button>
  //     </h2>
  //   </div>
  //
  //   <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
  //     <div class="card-body">
  //       Some placeholder content for the first accordion panel. This panel is shown by default, thanks to the <code>.show</code> class.
  //     </div>
  //   </div>
  // </div>


      })
      .catch(error=>{
        console.log(`Error2: Hubo un error al buscar Imagenes de Razas: ${error}`);
        alert(`Error2: Hubo un error al buscar Imagenes de Razas: ${error}`);
      });








    }
  })
  .catch(error=>{
    console.log(`Error1: Hubo un error al buscar Lista de Razas: ${error}`);
    alert(`Error1: Hubo un error al buscar Lista de Razas: ${error}`);
  });
});
