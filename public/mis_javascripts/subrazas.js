let urlApi='https://dog.ceo/api/';
let subrazas=[];

function verSubraza(nombreraza){
  subrazas=[];

  $('#lista-razas').hide(1000);
  $('#mensaje-error').hide(1000);
  $('#mensaje-espera').show(1000);
  $('#boton-volver').hide(1000);
  $('#subrazas').html("");
  $('#subrazas').hide();

  console.log(urlApi+`breed/${nombreraza}/list`);
  fetch(urlApi+`breed/${nombreraza}/list`)
  .then(respuesta=>{
    //console.log(respuesta);
    if(respuesta.statusText==='OK'){

      return respuesta.json();

		}


    return {"status":"error"};

  })
  .then(datos=>{

    // console.log('veamos los datos');
    // console.log(datos);

    if(datos.status.localeCompare("success")===0){

      if(datos.message.length>0){

        for(let k=0;k<datos.message.length;k++){
          let subraza={
            "nombre":datos.message[k],
            "imagen":""
          };

          subrazas.push(subraza);


        }

        //console.log(subrazas);

        let llamadas_imagenes=[];
        subrazas.forEach(subraza=>{
          llamadas_imagenes.push(fetch(`${urlApi}breed/${nombreraza}/${subraza.nombre}/images/random`).then(resp=>{return resp.json();}));
        });

        Promise.all(llamadas_imagenes)
        .then(imagenes_subrazas=>{
          imagenes_subrazas.forEach(imagen_subraza=>{
            if(imagen_subraza.status.localeCompare("success")===0){
              let arreglo_palabras=imagen_subraza.message.split("/");
              let nombre_desde_foto=arreglo_palabras[arreglo_palabras.length-2];
              let arreglo_palabras_subraza=nombre_desde_foto.split("-");
              let nombre_desde_foto_subraza=arreglo_palabras_subraza[1];
              subrazas.forEach(subraza=>{
                // if(raza.nombre.localeCompare(nombre_desde_foto)===0){
                //   raza.imagen=imagen_raza.message;
                // }
                console.log(subraza.nombre);

                if(nombre_desde_foto_subraza.startsWith(subraza.nombre)){
                  subraza.imagen=imagen_subraza.message;
                }

              });

              //console.log('Revisión necesaria 1');
              //console.log(subrazas);
            }
          });


          let contenedor= $('<div></div>').addClass("container");

          $('#subrazas').append(contenedor);
          let fila_nombre_raza=$('<div></div>').addClass("row");
          let encabezado_nombre_raza=$('<h2></h2>');
          let nombre_display=nombreraza[0].toUpperCase() + nombreraza.slice(1);
          encabezado_nombre_raza.html(`Raza: ${nombre_display}`);
          contenedor.append(encabezado_nombre_raza)
          contenedor.append('<br>')
          let fila= $('<div></div>').addClass("row justify-content-center");
          for(let k=0;k<subrazas.length;k++){
            //console.log(`veamos division: ${k%3}`);

            if(k%3==0){
              fila= $('<div></div>').addClass("row justify-content-center");
              contenedor.append(fila);
              let saltos=$('<div></div>').addClass('d-block d-sm-none d-none d-sm-block');
              saltos.html('<br><br>');
              contenedor.append(saltos);
            }

            let columna=$('<div></div>').addClass("col-sm-12 col-md-4 col-lg-4");

            let encabezado_columna=$('<h3></h3>');


            let subraza_nombre_display=subrazas[k].nombre[0].toUpperCase() + subrazas[k].nombre.slice(1);
            encabezado_columna.html(`${subraza_nombre_display}`);

            // let imagen_subraza=$('<img >')
            // imagen_subraza.attr('src', subrazas[k].imagen);

            let div_envoltorio=$('<div></div>').addClass('envoltorio-foto-subraza');

            /*para pantallas de sm a large*/
            let div_foto=$('<div></div>').addClass('d-none d-sm-block foto-subraza');
            div_foto.css('background-image',`linear-gradient(to top right, rgba(0,0,0,0.9), rgba(0,0,200,0.1)), url(${subrazas[k].imagen})`);

            /*para pantallas xs*/
            let div_foto_res=$('<div></div>').addClass('d-block d-sm-none foto-subraza-res');
            div_foto_res.css('background-image',`linear-gradient(to top right, rgba(0,0,0,0.9), rgba(0,0,200,0.1)), url(${subrazas[k].imagen})`);

            div_envoltorio.append(div_foto);
            div_envoltorio.append(div_foto_res);

            /*vamos a colocar algunos saltos para pantallas pequeñas*/



            let saltos_xs=$('<div></div>').addClass('d-block d-sm-none');
            saltos_xs.html('<br><br>');




            //columna.append(imagen_subraza);
            columna.append(div_envoltorio);
            columna.append(encabezado_columna);
            columna.append(saltos_xs);

            fila.append(columna);

          }



          $('#mensaje-espera').hide(1000);
          $('#boton-volver').show(1000);
          $('#subrazas').show(1000);




        })
        .catch(error=>{

          $("#mensaje-error").html(`Ocurrió un error: ${error}`);
          console.log(`Ocurrió un error: ${error}`);

          $('#mensaje-error').show(1000);
          $('#mensaje-espera').hide(1000);
          $('#boton-volver').show(1000);

        });


      }else{

          let nombre_display=nombreraza[0].toUpperCase() + nombreraza.slice(1);
        $("#mensaje-error").html(`Raza ${nombre_display} no tiene subrazas en esta API.`);
        $('#mensaje-error').show(1000);
        $('#mensaje-espera').hide(1000);
        $('#boton-volver').show(1000);
      }

    }else{

      $("#mensaje-error").html(`Ocurrió un error: Revisar link ${urlApi}breed/${nombreraza}/list`);
      $('#mensaje-error').show(1000);
      $('#mensaje-espera').hide(1000);
      $('#boton-volver').show(1000);
    }

  })
  .catch(error=>{

    $("#mensaje-error").html(`Ocurrió un error: ${error}`);
    console.log(`Ocurrió un error: ${error}`);

    $('#mensaje-error').show(1000);
    $('#mensaje-espera').hide(1000);
    $('#boton-volver').show(1000);

  });



}


function volver(){
  $('#lista-razas').show(1000);
  $('#mensaje-espera').hide(1000);
  $('#boton-volver').hide(1000);

  $('#mensaje-error').hide(1000);
  $('#subrazas').hide(1000);
}
