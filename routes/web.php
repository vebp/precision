<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $urlApi="https://dog.ceo/api/";
    $json = file_get_contents($urlApi."breeds/list/all");
    //echo $json["message"];
    $obj = json_decode($json,true);

    //var_dump($obj);
    //var_dump($obj['message']);
    // print_r( $obj['message']);
    //echo $obj['status'];
    //print $obj['message'];
    //debug_to_console($obj);

    $razas=array();
    foreach($obj['message'] as $key => $value) {
      //echo "raza: ".$key."\n";

      $json_imagen = file_get_contents($urlApi."breed/".$key."/images/random");
      //echo $json["message"];
      $obj_imagen = json_decode($json_imagen,true);
      //echo $obj_imagen['message'];

      array_push($razas, array(
          "nombre" => $key,
          "imagen" => $obj_imagen['message']));
       // foreach ($value as $subraza) {
       //   echo "subraza: ".$subraza."\n";
       // }
    }

    $letras=array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
    //
    return view('welcome',[
      'razas'=>$razas,
      'letras'=>$letras
    ]);
});


function debug_to_console($data, $context = 'Debug in Console') {

    // Buffering to solve problems frameworks, like header() in this and not a solid return.
    ob_start();

    $output  = 'console.info(\'' . $context . ':\');';
    $output .= 'console.log(' . json_encode($data) . ');';
    $output  = sprintf('<script>%s</script>', $output);

    echo $output;
}
