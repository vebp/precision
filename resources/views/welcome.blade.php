<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

	<title>Precisión</title>




	<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans&display=swap" rel="stylesheet">
<link rel="stylesheet" href="mis_css/estilo.css" />
<link rel="stylesheet" href="mis_css/estilo-subrazas.css" />

</head>

<body>

  <div id="lista-razas" class="container-fluid">
		<div class="accordion" id="acordeonRazas">

			<?php

			foreach($letras as $letra) {
				echo "<div class='card'>";
					echo '<div class="card-header acordeon-fila" id="encabezado-letra-'.$letra.'">';
						echo '<h2 class="mb-0">';
							echo '<button class="btn btn-link btn-block text-left acordeon-fila-boton" type="button" data-toggle="collapse" data-target="#colapso-letra-'.$letra.'" aria-expanded="false" aria-controls="colapso-letra-'.$letra.'">';
								echo strtoupper($letra);
							echo '</button>';
						echo '</h2>';
					echo "</div>";

					echo '  <div id="colapso-letra-'.$letra.'" class="collapse" aria-labelledby="encabezado-letra-'.$letra.'" data-parent="#acordeonRazas">';
						echo '<div class="card-body">';
							foreach($razas as $raza){
								if (str_starts_with($raza['nombre'], $letra)) {

									echo '<div class="container-fluid">';


										echo '<div class="row justify-content-center">';
											echo '<div class="col-sm-10 col-md-6">';
													echo "<a class='clase-a' onclick='verSubraza(&quot;".$raza['nombre']."&quot;)'>";
														echo '<div class="envoltorio-foto-raza">';

															#para pantallas de sm a large
															echo "<div class='d-none d-sm-block foto-raza' style='background-image:linear-gradient(to top right, rgba(0,0,0,0.9), rgba(0,0,200,0.1)), url(".$raza['imagen'].")'></div>";

															#para pantallas de xs

															echo "<div class='d-block d-sm-none foto-raza-res' style='background-image:linear-gradient(to top right, rgba(0,0,0,0.9), rgba(0,0,200,0.1)), url(".$raza['imagen'].")'></div>";

														echo '</div>';
													echo "</a>";

													echo '<a class="clase-a" onclick="verSubraza(&apos;'.$raza["nombre"].'&apos;)"><h3 class="encabezado">'.ucfirst($raza['nombre']).'</h3></a>';

											echo '</div>';
										echo '</div>';

									echo '</div>';

									echo "<br><br>";

								}

							}
						echo '</div>';
					echo '</div>';

				echo "</div>";

			}
			?>





</div>





  </div>

	<div class="container-fluid">
		<h3 id="mensaje-espera" style="display:none">Por favor espere... estamos descargando información</h3>
		<h3 id="mensaje-error" style="display:none"></h3>

		<div id="subrazas" style="display:none"></div>
		<br><br>
		<button id="boton-volver" onclick="volver()" style="display:none;" class="boton-volver">Volver</button>
	</div>






<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>


<!-- <script src="mis_javascripts/js-inicio.js" ></script> -->

<script src="mis_javascripts/subrazas.js" ></script>

</body>

</html>
